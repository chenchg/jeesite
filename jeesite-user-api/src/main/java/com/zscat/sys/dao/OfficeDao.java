/**
 * Copyright &copy; 2012-2016 <a href="http://git.oschina.net/wocadi/jeesite">JeeSite</a> All rights reserved.
 */
package com.zscat.sys.dao;

import com.zscat.common.persistence.TreeDao;
import com.zscat.common.persistence.annotation.MyBatisDao;
import com.zscat.common.persistence.sys.Office;

/**
 * 机构DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@MyBatisDao
public interface OfficeDao extends TreeDao<Office> {
	
}
